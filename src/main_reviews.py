import os
import re
import sys
from datetime import datetime, timedelta

import nltk
import numpy as np
from nltk import WordNetLemmatizer
from pyspark.ml.clustering import LDA
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import Tokenizer, StopWordsRemover, MinHashLSH, CountVectorizer, IDF, HashingTF, MinMaxScaler
from pyspark.ml.functions import vector_to_array
from pyspark.ml.recommendation import ALS
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.sql import SparkSession, Window
from pyspark.sql.functions import col, lower, udf, explode, collect_list, regexp_extract, transform, \
    max as mx, expr, sum as sm, \
    add_months, date_sub, regexp_replace, split, lit, row_number, count, avg
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType, DateType, ArrayType
from pyspark.ml.linalg import VectorUDT

from textblob import TextBlob
from langdetect import detect

from nltk.corpus import words,stopwords
from nltk.sentiment.vader import SentimentIntensityAnalyzer
#nltk.download("words")
nltk.download("stopwords")
nltk.download("wordnet")
#nltk.download("vader_lexicon")
wltizer = WordNetLemmatizer()
#sid = SentimentIntensityAnalyzer()
wrd = set(words.words())
stopwrd = set(stopwords.words('english'))

from itertools import chain

# Our hope is to determine the following from the AirBnB dataset:
# -> What are the most useful names, host location, and comments for determining Airbnb ratings data
# -> The best recommendations for places within the United States
# -> OPTIONAL (if there is time): If locality affects the Airbnb Host Response Rate

#There are three stated tools associated with this project
# 1) Utilizing Local Sensitivity Hashing for the purpose of appropriately bucketing AirBNB reviews
# 2) Information from LDA will be processed to determine the most useful topics associated with AirBnB comments
# 3) Determine which USA locations are the best suited for users based on the ratings listed

def sentiment_analysis(text):
    try:
        if detect(text) != "en":
            return -2
    except:
        return -2
    tsp = float(TextBlob(text).sentiment.polarity)
    #print("Sentiment Polarity: "+str(tsp))
    return tsp

def lemma(x):
    return wltizer.lemmatize(x) if x.isalpha() and x in wrd else ''

def print_val(x,data,i):
    retVal = str("==== Topic "+str(i)+" Words ====\n")
    data_i = str(x)
    retVal = retVal+data_i
    data_i = re.sub("(\\[|\\]|\\')","",data_i.replace(",", "|").replace(" ",""))
        #re.sub("(|\[|\]|\')","",data_i.replace(",", "|").replace(" ",""))
    data_i = ".*(" + data_i + ").*"
    retVal = retVal+("\n=== REGEX USED === \n")+data_i+"\n"
    retVal = retVal+("\n=== Topic "+str(i)+" Word Examples ===\n")
    data_i_ex = filter(lambda x : re.search(data_i, str(x).lower()),data)
    retVal = retVal + next(data_i_ex, "(Cannot find an example for topic)") +"\n"
    retVal = retVal + next(data_i_ex, "(Cannot find an example for topic)") + "\n"
    retVal = retVal + next(data_i_ex, "(Cannot find an example for topic)") + "\n"
    retVal = retVal + next(data_i_ex, "(Cannot find an example for topic)") + "\n"
    retVal = retVal + next(data_i_ex, "(Cannot find an example for topic)") + "\n"
    return retVal

def LDA_processing(comment_reviews,ref_dir, enablePYLDAvis=True): #
    print("=== PROCESSING LDA ===")
    output_dir = ref_dir + os.sep + "output" + os.sep
    ret_source = comment_reviews.select(col("comments").alias("words")).collect()
    ret_comments = list(map(lambda x : x["words"],ret_source))

    cv = CountVectorizer(inputCol="cleaned_comment_words", outputCol="cv_words")
    comment_review_format = cv.fit(comment_reviews.drop(col("cv_words")))
    crf_vocab = comment_review_format.vocabulary

    """
    idf = IDF(inputCol="cv_words", outputCol="cvidf_words")
    idf_format = idf.fit(comment_reviews)
    comment_reviews = idf_format.transform(comment_reviews)
    comment_reviews = comment_reviews.drop(col("cv_words")).withColumnRenamed("cvidf_words","cv_words")
    """

    lda_model = LDA(k=40, learningDecay=0.1,seed=202, featuresCol="cv_words")
    lda_fit = lda_model.fit(comment_reviews)
    #lda_output = lda_fit.transform(comment_reviews)

    print("Finished LDA... ")

    lda_desc_topics = lda_fit.describeTopics().select("topic","termIndices").collect()
    lda_desc_topics = list(map(lambda x: (x["topic"], list(map(lambda y: str(crf_vocab[y]), x["termIndices"]))), lda_desc_topics))
    #print value is taking up a lot of time. May require threading
    lda_tpcs = list(map(lambda x : print_val(x[1],ret_comments,x[0]),lda_desc_topics))
    print("\n".join(lda_tpcs))

    """
    print("Finished printing LDA values... ")

    if enablePYLDAvis == True:
        print("Providing pyLDAvis data...")
        win_rn = Window.partitionBy("iterate").orderBy("iterate")
        lda_output = lda_output.withColumn("iterate", lit(1))
        lda_output = lda_output.withColumn("row_num", row_number().over(win_rn))
        lda_output = lda_output.drop(col("iterate"))

        lda_total_words = lda_output.select(col("row_num"),explode("cleaned_comment_words").alias("words"))
        lda_total_words = lda_total_words.groupBy(col("row_num")).agg(count(col("words")).alias("total_words_count"))

        lda_obj_tf_frequency = lda_output.select(explode("cleaned_comment_words").alias("words"))
        lda_obj_tf_frequency = lda_obj_tf_frequency.groupBy(col("words")).agg(count("words").alias("freq"))

        lda_obj_tf_frequency = lda_obj_tf_frequency.collect()
        lda_obj_tf_frequency = dict(map(lambda x : (x["words"],x["freq"]),lda_obj_tf_frequency))

        topics = np.asarray(lda_fit.topicsMatrix().toArray()).T

        allowed_row_nums = lda_output.select(col("row_num"),explode(vector_to_array(col("topicDistribution"))).alias("topic_dist"))
        allowed_row_nums = allowed_row_nums.groupBy(col("row_num")).agg(sm(col("topic_dist")).alias("topic_dist_sum"))
        allowed_row_nums = allowed_row_nums.filter(col("topic_dist_sum") == 1).collect()
        allowed_row_nums = list(map(lambda x: x["row_num"], allowed_row_nums))

        docTerm = lda_output.filter(col("row_num").isin(allowed_row_nums)).select(col("topicDistribution")).collect()
        lda_total_words = lda_total_words.filter(col("row_num").isin(allowed_row_nums)).select(col("row_num"),
                                                                                               col("total_words_count")).orderBy(
            col("row_num")).collect()
        docTerm = np.array(list(map(lambda x: x["topicDistribution"].toArray(), docTerm)))
        docLen = np.array(list(map(lambda x: x["total_words_count"], lda_total_words)))
        vocab_freq = np.array(list(map(lambda x: lda_obj_tf_frequency[x], crf_vocab)))

        print("Finished pylDAvis Prep... ")
        try:
            import pyLDAvis
            from pyLDAvis import sklearn
            prepped = pyLDAvis.prepare(topics, docTerm, docLen, crf_vocab, vocab_freq)
            pyLDAvis.save_html(prepped, output_dir+"LDA_Results.html")
            print("Finished pyLDAvis... ")
        except Exception as e:
            print("Could not execute pyLDAvis: "+str(e))
    """



def CF_processing(spark): #id,reference_path,
    """
    review_schema = StructType([ \
        StructField("listing_id", IntegerType(), False), \
        StructField("reviewer_id", IntegerType(), False), \
        StructField("comments", StringType(), False), \
        StructField("cleaned_comment_words", ArrayType(StringType()), False), \
        StructField("hashes_id", StringType(), False), \
        StructField('cv_words', VectorUDT(), False), \
        StructField('sentiment_analysis', FloatType(), False)
        ])
    """
    try:
        #comment_reviews = spark.sql("select hashes_id, listing_id, reviewer_id, cleaned_comment_words from hash_content where hashes_id="+str(id))
        #print("Hash Id Used: "+str(id))
        #print("Reference Path: "+reference_path + os.sep + "spark-warehouse" + os.sep + "hash_content" + os.sep + "hashes_id=" + str(id) + os.sep + "*.parquet")
        #str_path = reference_path + os.sep + "spark-warehouse" + os.sep + "hash_content" + os.sep + "hashes_id=" + str(id) + os.sep + "*.parquet"
        #comment_reviews = spark.read.parquet(str_path,schema=review_schema)
        comment_reviews = spark.sql("SELECT * FROM hash_content")
        comment_reviews = comment_reviews\
            .filter(col("reviewer_id").isNotNull())\
            .filter(col("listing_id").isNotNull())\
            .filter(col("sentiment_analysis").isNotNull())
        #if comment_reviews.isEmpty():
        #    return []

        """
        mh = MinHashLSH(inputCol="cv_words", outputCol="hashes", seed=202, numHashTables=3)
        minhash_fit = mh.fit(comment_reviews)

        print("===== PRE-PLANNING ITEM-ITEM CF PRIOR TO CONTINUING =====")
        comment_reviews_only = comment_reviews.select(col("hashes_id"), col("listing_id"), col("reviewer_id"), \
                                                      col("comments"),col("cv_words"),\
                                                      col("sentiment_analysis").alias("rating"))
        comment_reviews_dup = comment_reviews.select(col("listing_id").alias("l2"), col("reviewer_id").alias("r2"), \
                                                     col("comments").alias("c2"),col("cv_words"),\
                                                     col("sentiment_analysis").alias("rating"))

        comment_reviews_recorder = minhash_fit.approxSimilarityJoin(comment_reviews_only, \
                                                                    comment_reviews_dup, \
                                                                    0.1, distCol="CosineDistance")
        print("Completed minHash For Item-Item CF...")
        comment_reviews_recorder = comment_reviews_recorder.select(
            col("datasetA.hashes_id").alias("hashes_id"), \
            col("datasetA.listing_id").alias("const_listing"), \
            col("datasetB.l2").alias("sim_listing"), \
            col("datasetA.reviewer_id").alias("reviewer_id"), \
            col("datasetB.r2").alias("r2"), \
            col("datasetA.comments").alias("comments"), \
            col("datasetB.c2").alias("comments_2"), \
            col("datasetA.rating").alias("rating"), \
            col("datasetB.rating").alias("rating_2"), \
            col("CosineDistance").alias("similarity_score"))
        print("Filtering options for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.filter(
            comment_reviews_recorder.reviewer_id == comment_reviews_recorder.r2) \
            .filter(comment_reviews_recorder.const_listing < comment_reviews_recorder.sim_listing) \
            .filter(comment_reviews_recorder.rating_2.isNotNull()) \
            .filter(comment_reviews_recorder.rating.isNotNull()).drop(col("r2")).dropna()

        item_id_spec = Window.partitionBy(col("const_listing"))
        item_id2_spec = Window.partitionBy(col("sim_listing"))
        item_spec = Window.partitionBy(col("const_listing"), col("sim_listing"))
        usr_rat_win = Window.partitionBy(col("reviewer_id"))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("sum_sim_ij", \
                                                                       sm(col("similarity_score")).over(item_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("user_reviews", \
                                                                       count(col("reviewer_id")).over(usr_rat_win))

        coOccurenceThreshold = 40
        print("Getting averages for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.filter(col("user_reviews") >= coOccurenceThreshold)
        avg_rating = comment_reviews_recorder.groupBy(col("rating")).agg(avg("rating").alias("avg_rating")).first()[
            "avg_rating"]
        comment_reviews_recorder = comment_reviews_recorder.withColumn("usr_avg_ratings", \
                                                                       avg(col("rating")).over(usr_rat_win))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("avg_rating", lit(avg_rating))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("usr_avg_ratings", \
                                                                       col("usr_avg_ratings") - col("avg_rating"))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("item_j_avg_ratings", \
                                                                       avg(col("rating")).over(item_id_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("item_i_avg_ratings", \
                                                                       avg(col("rating_2")).over(item_id2_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("bxi", col("avg_rating") + ( \
                    col("usr_avg_ratings") - col("avg_rating")) + (col("item_i_avg_ratings") - col("avg_rating")))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("bxj", col("avg_rating") + ( \
                    col("usr_avg_ratings") - col("avg_rating")) + (col("item_j_avg_ratings") - col("avg_rating")))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("numerator", sm(col("similarity_score") * (
                    col("rating") - col("bxj"))).over(item_id_spec))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("numerator", sm(col("similarity_score") * (
                    col("rating") - col("bxj"))).over(item_id_spec))

        print("Getting r_pred for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.withColumn("r_pred", (
                    col("bxi") + (col("numerator") / col("sum_sim_ij"))))
        comment_reviews = comment_reviews_recorder.select(col("reviewer_id"), col("sim_listing").alias("listing_id"),
                                                                   col("comments"), col("rating_2").alias("sentiment_analysis"),
                                                                   col("r_pred").alias("itemCF_prediction"))
        """
        print("===== FINISHED ITEM-ITEM CF =====")

        # This is where we have to start our recommender system journey
        als_format = ALS(seed=202, coldStartStrategy="drop", nonnegative=True, rank=10, regParam=0.3,userCol="reviewer_id",itemCol="listing_id",ratingCol="sentiment_analysis")
        als_format = als_format.setImplicitPrefs(False)

        trainingId, testId = comment_reviews.randomSplit([0.8, 0.2], seed=202)
        trainingId = trainingId.cache()
        testId = testId.cache()

        pg = ParamGridBuilder().addGrid(als_format.rank, [10, 11, 12]).addGrid(als_format.regParam,[0.1, 0.5, 0.7]).build()
        re_eval = RegressionEvaluator(labelCol="sentiment_analysis", predictionCol="prediction")
        cv = CrossValidator(estimator=als_format,
                            estimatorParamMaps=pg,
                            evaluator=re_eval,
                            numFolds=5) #number of folds is best with 10
        print("ATTEMPTING TO FIT ALS")
        als_fit = cv.fit(trainingId)
        als_fit = als_fit.bestModel

        #print("ATTEMPTING TO FIT ALS")
        #als_fit = als_format.fit(comment_reviews)
        print("ATTEMPTING TO TRANSFORM ALS")
        testId = als_fit.transform(testId)
        #comment_reviews = comment_reviews.withColumnRenamed("prediction","ALS_prediction")
        #comment_reviews = comment_reviews.withColumn("prediction",(col("ALS_prediction") * 0.3) + (col("itemCF_prediction") * 0.7))

        #comment_reviews = comment_reviews.select(col("rating"),col("hybrid_pred")).filter(col("rating").isNotNull()).filter(col("hybrid_pred").isNotNull())

        print("======Recommender Results======")
        reg_eval = RegressionEvaluator(metricName="rmse", labelCol="sentiment_analysis", predictionCol="prediction")
        ratings_rmse = reg_eval.evaluate(testId)
        print("Ratings RMSE: " + str(ratings_rmse))

        reg_eval = RegressionEvaluator(metricName="mse", labelCol="sentiment_analysis", predictionCol="prediction")
        ratings_mse = reg_eval.evaluate(testId)
        print("Ratings MSE: "+str(ratings_mse))

        reg_eval = RegressionEvaluator(metricName="r2", labelCol="sentiment_analysis", predictionCol="prediction")
        ratings_r2 = reg_eval.evaluate(testId)
        print("Ratings R2: " + str(ratings_r2))

        print("=== OUR USER RECOMMENDATIONS FOR FIVE ITEMS ===")
        results = als_fit.recommendForAllItems(5)
        results.show()

        return ["Ratings RMSE: " + str(ratings_rmse), "Ratings MSE: "+str(ratings_mse), "Ratings R2: " + str(ratings_r2)]
    except Exception as e:
        print("ERROR: "+str(e))
        return []

def clean(comment_identify_reviews):
    comment_identify_reviews = comment_identify_reviews.select(col("listing_id"), col("reviewer_id"), col("comments"))

    # comment_reviews_count = comment_identify_reviews.count()
    # print("=============== PRE-PRE-PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")
    # return

    # Uncomment later for sentiment analysis
    udf_lemma = udf(lambda x: "" + lemma(x), StringType())

    tk = Tokenizer(inputCol="comments", outputCol="tokenized_comments")
    comment_token_ratings = tk.transform(comment_identify_reviews)

    stw = StopWordsRemover(inputCol="tokenized_comments", outputCol="comment_words", stopWords=list(stopwrd))
    comment_tweet = stw.transform(comment_token_ratings)
    comment_reviews = comment_tweet.drop(col("tokenized_comments"))

    comment_reviews = comment_reviews.select(col("listing_id"), col("reviewer_id"), col("comments"),
                                             explode(col("comment_words")).alias("comments_rework"))
    # comment_reviews = comment_reviews.withColumn("comments_rework",udf_lemma(col("comment_words"))).filter(col("comments_rework") != "").drop(col("comment_words"))

    print(
        "First, it's a good idea to partition based on the comments. This'll be useful for our LDA, but also our minhash and recommender later")
    comment_reviews = comment_reviews.withColumn("cr_words", udf_lemma(
        lower(regexp_extract(col("comments_rework"), r"([A-Za-z]+)", 1)))).drop(col("comments_rework"))
    comment_reviews = comment_reviews.filter(col("cr_words") != "")
    comment_reviews = comment_reviews.groupBy(col("listing_id"), col("reviewer_id"), col("comments")).agg(
        collect_list(col("cr_words")).alias("cleaned_comment_words"))

    # comment_reviews_count = comment_reviews.count()
    # print("=============== PRE-PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")

    comment_reviews = comment_reviews.distinct().dropna().cache()

    print("==== BEGINNING STREAMING SAVE ====")

    cv = CountVectorizer(inputCol="cleaned_comment_words", outputCol="cv_words")
    comment_review_format = cv.fit(comment_reviews)
    comment_reviews = comment_review_format.transform(comment_reviews)  # .drop(col("cleaned_comment_words"))

    mh = MinHashLSH(inputCol="cv_words", outputCol="hashes", seed=202, numHashTables=3)
    minhash_fit = mh.fit(comment_reviews)
    comment_reviews = minhash_fit.transform(comment_reviews).cache()

    udf_sa = udf(lambda x: sentiment_analysis(x), FloatType())
    comment_reviews = comment_reviews.filter(col("cleaned_comment_words").isNotNull())
    comment_reviews = comment_reviews.withColumn("sentiment_analysis", udf_sa(col("comments")))

    comment_reviews = comment_reviews.filter(col("sentiment_analysis") != -2)
    comment_reviews = comment_reviews.filter(col("sentiment_analysis").isNotNull())

    print("Transferring hashes...")
    udf_hash = udf(lambda x: "" + str(x[0][0]))
    comment_reviews = comment_reviews.withColumn("hashes_id", udf_hash(col("hashes")))
    comment_reviews = comment_reviews.drop(col("hashes"))
    return comment_reviews

def central(ref_dir, mode=2, enablePYLDAvis = True):
    spark = None
    try:
        print("Starting data cleaning...")
        references_dir = ref_dir + os.sep + "spark-warehouse"
        input_ratings_file = ref_dir + os.sep + "input" + os.sep + "airbnb-reviews.csv"
        output_dir = ref_dir + os.sep + "output"
        print("First step is to try to condense this down only into the United States")
        #listing_id;id;date;reviewer_id;reviewer_name;comments
        review_schema = StructType([\
            StructField("listing_id", IntegerType(), False),\
            StructField("id",IntegerType(), False),\
            StructField("date",DateType(), False),\
            StructField("reviewer_id",IntegerType(), False),\
            StructField("reviewer_name",StringType(), False),\
            StructField("comments",StringType(), False)
        ])

        #FOR TESTING PURPOSES ONLY:
        # -> CSV file will be put forward containing all values listed. (Current process takes 10+ minutes)
        # -> Information will be handled in tested fully
        # -> Any reference to a CSV file will be ignored after completion

        sanity_check = False
        comment_reviews = None
        try:
            spark = SparkSession.builder.master("local[*]") \
                .config("spark.cores.max", "5") \
                .config("spark.driver.memory", "10g") \
                .config("spark.sql.warehouse.dir", references_dir) \
                .enableHiveSupport() \
                .appName("final_joe").getOrCreate()
            hashes_references = spark.read.csv(ref_dir+os.sep+"hashes_references",header=True)
            hashes_references.createOrReplaceTempView("hashes_references")
            # spark.sql("select * from hash_content limit where hashes_id=154880.0 ").show()
            print("=== WE FOUND THE HASH_REFERENCES! WORKING THROUGH THIS! ===")
            sanity_check = True
        except:
            print("=== WE DID NOT FIND THE HASH_REFERENCES! WILL RUN MinHashLSH AND TERMINATE FOR RE-RUN ===")
            if spark is not None:
                spark.stop()
            spark = SparkSession.builder.master("local[*]") \
                .config("spark.cores.max", "2") \
                .config("spark.driver.memory", "10g") \
                .appName("final_joe").getOrCreate()


        if sanity_check is False:
            print("PARQUET FILES NOT FOUND INITIALLY: WE MUST RUN THE APPLICATION TO PRODUCE THE PARQUET FILES FOR LATER STREAMING...")
            print("-> Restart the program after termination...")
            comment_identify_reviews = spark.read.csv(input_ratings_file, schema=review_schema, sep=";")

            print("Second step is to filter out the languages to only be in English")
            print("  --> This is where the cleaning aspect of our language comes.")
            print("  --> Sentiment Analysis and TFIDFVectorization is performed here.")
            #rev_sample = reviews.withColumnRenamed("listing_id","listing_id_2")
            #comment_identify_reviews = ratings.join(rev_sample, on=ratings.listing_id == rev_sample.listing_id_2, how="inner").drop("listing_id_2")

            #("2016-01-01","2017-01-01") takes a lot of values, and took over 2 horus and 30 minutes to produce. Reducing to only the summer.
            comment_identify_reviews = comment_identify_reviews.filter(col("comments") != "").filter(col("date").between("2017-01-01","2022-01-01")) #.limit(750_000)

            comment_reviews = clean(comment_identify_reviews)

            #comment_reviews_count = comment_reviews.count()
            #print("=============== PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")

            hash_write = comment_reviews.select(col("hashes_id")).distinct()

            #comment_reviews_count = comment_reviews.count()
            #print("=============== Comment Reviews Count: "+str(comment_reviews_count) +"===============")
            #return

            #comment_reviews =comment_reviews.withColumn("hashes_id", col("hashes").cast(StringType())).drop(col("hashes"))
            #comment_reviews = comment_reviews.groupBy(col("hashes_id"),col("listing_id"),col("reviewer_id")).agg(collect_list(col("cleaned_comment_words")))

            try:
                hash_write.coalesce(1).write.option("header",True).mode("overwrite").csv(ref_dir+os.sep+"hashes_references")

                #comment_reviews = comment_reviews.drop(col("cv_words"))
                comment_reviews = comment_reviews.select(col("listing_id"),col("reviewer_id"),col("comments"),col("cleaned_comment_words"),\
                                                         col("hashes_id"),col("cv_words"),col("sentiment_analysis"))

                comment_reviews.write.option("header",True)\
                    .partitionBy("hashes_id")\
                    .mode("overwrite")\
                    .saveAsTable("hash_content")

                """
                comment_reviews.write.option("header",True) \
                    .bucketBy(15,"hashes")\
                    .mode("overwrite")\
                    .saveAsTable("hash_content")
                """
            except Exception as e:
                print("Likely already has the spark read directory available for hashes content directory: "+str(e))

            print("Fully computed grouped values...")

            print("==== ENDING STREAMING SAVE ====")
            print("PLEASE RESTART THIS SYSTEM TO ANALYZE THE REST OF THE VALUES LISTED")

            spark.stop()
            return

        if mode == 1:
            print("=== CF PROCESS IS BEING RUN ===")
            #l_col_hash = hashes_references.collect()
            #resultant = list(map(lambda li : CF_processing(li["hashes_id"],ref_dir, spark), l_col_hash))
            resultant = CF_processing(spark)
            print("=== CF PROCESS IS COMPLETED; NOW SHOWING ===")
            #print("\n".join(resultant))
            print(str(resultant))
            #resultant.show()
            print("=== CF PROCESS COMPLETED ENTIRELY ===")
        elif mode == 2:
            print("=== LDA PROCESS IS BEING RUN ===")
            comment_reviews = spark.sql("SELECT * FROM hash_content")
            LDA_processing(comment_reviews,ref_dir, enablePYLDAvis)



    except Exception as e:
        print("An exception occurred: "+str(e))
    finally:
        print("Terminated Spark Instance")
        if spark is not None:
            spark.stop()

if __name__ == '__main__':
    print("For MODE...")
    print(" 1) CF Processing")
    print(" 2) LDA Processing (Default)")
    print("Keep in mind: If minHash has not been included, it will be created and then used")
    try:
        if len(sys.argv) < 2:
            print("Requiring at least one arguments, as such: ")
            print("python main_review.py <user directory> <mode> <enable pyLDAvis>")
            print("For MODE...")
            print(" 1) CF Processing")
            print(" 2) LDA Processing")
            central("/user/jbrock2")
        elif len(sys.argv) < 3:
            dir_val = str(sys.argv[1])
            central(dir_val)
        elif len(sys.argv) < 4:
            dir_val = str(sys.argv[1])
            mode = int(sys.argv[2])
            central(dir_val,mode)
        elif len(sys.argv) < 5:
            dir_val = str(sys.argv[1])
            mode = int(sys.argv[2])
            en_pyl = str(sys.argv[3]).lower() == 'true'
            central(dir_val, mode, en_pyl)
        else:
            a_dir = str(sys.argv[1])
            central(a_dir)
    except:
        print("Cannot process; please process with the following arguments")
        print("python main_review.py <user directory> <mode> <enable pyLDAvis>")

"""
            #REGARDLESS OF WHETHER OR NOT WE ACTUALLY USE IT, it's nice to have everything taken care of before running it through our evaluator
        mh = MinHashLSH(inputCol="cv_words", outputCol="hashes", seed=202, numHashTables=3)
        minhash_fit = mh.fit(comment_reviews)

        print("===== PRE-PLANNING ITEM-ITEM CF PRIOR TO CONTINUING =====")
        comment_reviews_only = comment_reviews.select(col("hashes_id"), col("listing_id"), col("reviewer_id"), \
                                                      col("comments"),col("cv_words"),\
                                                      col("sentiment_analysis").alias("rating"))
        comment_reviews_dup = comment_reviews.select(col("listing_id").alias("l2"), col("reviewer_id").alias("r2"), \
                                                     col("comments").alias("c2"),col("cv_words"),\
                                                     col("sentiment_analysis").alias("rating"))

        comment_reviews_recorder = minhash_fit.approxSimilarityJoin(comment_reviews_only, \
                                                                    comment_reviews_dup, \
                                                                    0.1, distCol="CosineDistance")
        print("Completed minHash For Item-Item CF...")
        comment_reviews_recorder = comment_reviews_recorder.select(
            col("datasetA.hashes_id").alias("hashes_id"), \
            col("datasetA.listing_id").alias("const_listing"), \
            col("datasetB.l2").alias("sim_listing"), \
            col("datasetA.reviewer_id").alias("reviewer_id"), \
            col("datasetB.r2").alias("r2"), \
            col("datasetA.comments").alias("comments"), \
            col("datasetB.c2").alias("comments_2"), \
            col("datasetA.rating").alias("rating"), \
            col("datasetB.rating").alias("rating_2"), \
            col("CosineDistance").alias("similarity_score"))
        print("Filtering options for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.filter(
            comment_reviews_recorder.reviewer_id == comment_reviews_recorder.r2) \
            .filter(comment_reviews_recorder.const_listing < comment_reviews_recorder.sim_listing) \
            .filter(comment_reviews_recorder.rating_2.isNotNull()) \
            .filter(comment_reviews_recorder.rating.isNotNull()).drop(col("r2")).dropna()

        item_id_spec = Window.partitionBy(col("const_listing"))
        item_id2_spec = Window.partitionBy(col("sim_listing"))
        item_spec = Window.partitionBy(col("const_listing"), col("sim_listing"))
        usr_rat_win = Window.partitionBy(col("reviewer_id"))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("sum_sim_ij", \
                                                                       sm(col("similarity_score")).over(item_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("user_reviews", \
                                                                       count(col("reviewer_id")).over(usr_rat_win))

        coOccurenceThreshold = 40
        print("Getting averages for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.filter(col("user_reviews") >= coOccurenceThreshold)
        avg_rating = comment_reviews_recorder.groupBy(col("rating")).agg(avg("rating").alias("avg_rating")).first()[
            "avg_rating"]
        comment_reviews_recorder = comment_reviews_recorder.withColumn("usr_avg_ratings", \
                                                                       avg(col("rating")).over(usr_rat_win))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("avg_rating", lit(avg_rating))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("usr_avg_ratings", \
                                                                       col("usr_avg_ratings") - col("avg_rating"))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("item_j_avg_ratings", \
                                                                       avg(col("rating")).over(item_id_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("item_i_avg_ratings", \
                                                                       avg(col("rating_2")).over(item_id2_spec))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("bxi", col("avg_rating") + ( \
                    col("usr_avg_ratings") - col("avg_rating")) + (col("item_i_avg_ratings") - col("avg_rating")))
        comment_reviews_recorder = comment_reviews_recorder.withColumn("bxj", col("avg_rating") + ( \
                    col("usr_avg_ratings") - col("avg_rating")) + (col("item_j_avg_ratings") - col("avg_rating")))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("numerator", sm(col("similarity_score") * (
                    col("rating") - col("bxj"))).over(item_id_spec))

        comment_reviews_recorder = comment_reviews_recorder.withColumn("numerator", sm(col("similarity_score") * (
                    col("rating") - col("bxj"))).over(item_id_spec))

        print("Getting r_pred for Item-Item CF..")
        comment_reviews_recorder = comment_reviews_recorder.withColumn("r_pred", (
                    col("bxi") + (col("numerator") / col("sum_sim_ij"))))
        comment_reviews = comment_reviews_recorder.select(col("reviewer_id"), col("sim_listing").alias("listing_id"),
                                                                   col("comments"), col("rating_2").alias("rating"),
                                                                   col("r_pred").alias("itemCF_prediction"))

        print("===== FINISHED ITEM-ITEM CF =====")

"""

"""
        comment_reviews_recorder.write.option("header",True)\
            .partitionBy("hashes_id")\
            .mode("overwrite")\
            .saveAsTable("item_item_CF")
"""
