import os
import re
import sys
from datetime import datetime, timedelta

import nltk
import numpy as np
from nltk import WordNetLemmatizer
from pyspark.ml.clustering import LDA
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import Tokenizer, StopWordsRemover, MinHashLSH, CountVectorizer, IDF, HashingTF, MinMaxScaler
from pyspark.ml.functions import vector_to_array
from pyspark.ml.recommendation import ALS
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.sql import SparkSession, Window
from pyspark.sql.functions import col, lower, udf, explode, collect_list, regexp_extract, transform, \
    max as mx, expr, sum as sm, when, \
    add_months, date_sub, regexp_replace, split, lit, row_number, count, avg
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType, DateType, ArrayType
from pyspark.ml.linalg import VectorUDT
from pyspark.ml.evaluation import RegressionEvaluator, BinaryClassificationEvaluator
from textblob import TextBlob
from langdetect import detect
from pyspark.ml.classification import LogisticRegression
from nltk.corpus import words,stopwords
from nltk.sentiment.vader import SentimentIntensityAnalyzer

#nltk.download("words")
nltk.download("stopwords")
nltk.download("wordnet")
#nltk.download("vader_lexicon")
wltizer = WordNetLemmatizer()
#sid = SentimentIntensityAnalyzer()
wrd = set(words.words())
stopwrd = set(stopwords.words('english'))

from itertools import chain

# Our hope is to determine the following from the AirBnB dataset:
# -> What are the most useful names, host location, and comments for determining Airbnb ratings data
# -> The best recommendations for places within the United States
# -> OPTIONAL (if there is time): If locality affects the Airbnb Host Response Rate

#There are three stated tools associated with this project
# 1) Utilizing Local Sensitivity Hashing for the purpose of appropriately bucketing AirBNB reviews
# 2) Information from LDA will be processed to determine the most useful topics associated with AirBnB comments
# 3) Locations are the best suited for users based on the ratings listed

def logistic_reg(spark,ref_dir="/user/jbrock2/spark-warehouse/",hash_id=None):
    print("======== RETRIEVING HASH ID ========")
    if hash_id is None:
        hashDF = spark.sql("SELECT * FROM log_hash_content")
        #print("======= LOG HASH SIZE DF "+ str(hashDF.count()) +" =======")
        #return
    else:
        hash_list = list(chain(hash_id))
        hash_list = list(map(lambda x : str(float(x[0])),hash_list))
        hash_list = "("+",".join(hash_list) + ")"
        #print(str(hash_list))
        hashDF = spark.sql("SELECT * FROM log_hash_content WHERE hashes_id IN "+hash_list) #.filter(col("hashes_id").isin(hash_list))

    hashDF = hashDF.withColumn("iterate",lit(1))
    win_rn = Window.partitionBy("iterate").orderBy("iterate")
    hashDF = hashDF.withColumn("row_num", row_number().over(win_rn))
    hashDF = hashDF.withColumn("label", when(col("sentiment_analysis") >= 0, 1).otherwise(0))
    train_job = hashDF.sample(fraction=0.7, seed=202)
    test_job = hashDF.drop(train_job.row_num)
    train_job_pre = train_job.select(col("cv_words").alias("features"), col("label"))
    test_job_pre = test_job.select(col("cv_words").alias("features"), col("label"))
    lr = LogisticRegression()
    #paramGrid = ParamGridBuilder() \
    #    .addGrid(lr.regParam, [0.1, 0.01]).build()  # Remove numFeats if needed

    print("Working through Logistic Regression...")
    #cv = CrossValidator(estimator=lr,
    #                    estimatorParamMaps=paramGrid,
    #                    evaluator=BinaryClassificationEvaluator(),
    #                    numFolds=5, parallelism=5)
    lrModelTrain = lr.fit(train_job_pre)
    lrModelTest = lrModelTrain.transform(test_job_pre)
    lrROCClass = BinaryClassificationEvaluator(rawPredictionCol="rawPrediction", metricName="areaUnderROC")
    resultant_ROC = lrROCClass.evaluate(lrModelTest)
    print("=== LR Model Evaluation Result: "+str(resultant_ROC)+" ===")
    return float(resultant_ROC)


def sentiment_analysis(text):
    try:
        if detect(text) != "en":
            return -2
    except:
        return -2
    tsp = float(TextBlob(text).sentiment.polarity)
    #print("Sentiment Polarity: "+str(tsp))
    return tsp

def lemma(x):
    return wltizer.lemmatize(x) if x.isalpha() and x in wrd else ''

def clean(comment_identify_reviews):
    comment_identify_reviews = comment_identify_reviews.select(col("listing_id"), col("reviewer_id"), col("comments"))

    # comment_reviews_count = comment_identify_reviews.count()
    # print("=============== PRE-PRE-PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")
    # return

    # Uncomment later for sentiment analysis
    udf_lemma = udf(lambda x: "" + lemma(x), StringType())

    tk = Tokenizer(inputCol="comments", outputCol="tokenized_comments")
    comment_token_ratings = tk.transform(comment_identify_reviews)

    stw = StopWordsRemover(inputCol="tokenized_comments", outputCol="comment_words", stopWords=list(stopwrd))
    comment_tweet = stw.transform(comment_token_ratings)
    comment_reviews = comment_tweet.drop(col("tokenized_comments"))

    comment_reviews = comment_reviews.select(col("listing_id"), col("reviewer_id"), col("comments"),
                                             explode(col("comment_words")).alias("comments_rework"))
    # comment_reviews = comment_reviews.withColumn("comments_rework",udf_lemma(col("comment_words"))).filter(col("comments_rework") != "").drop(col("comment_words"))

    print(
        "First, it's a good idea to partition based on the comments. This'll be useful for our LDA, but also our minhash and recommender later")
    comment_reviews = comment_reviews.withColumn("cr_words", udf_lemma(
        lower(regexp_extract(col("comments_rework"), r"([A-Za-z]+)", 1)))).drop(col("comments_rework"))
    comment_reviews = comment_reviews.filter(col("cr_words") != "")
    comment_reviews = comment_reviews.groupBy(col("listing_id"), col("reviewer_id"), col("comments")).agg(
        collect_list(col("cr_words")).alias("cleaned_comment_words"))

    # comment_reviews_count = comment_reviews.count()
    # print("=============== PRE-PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")

    comment_reviews = comment_reviews.distinct().dropna().cache()

    print("==== BEGINNING STREAMING SAVE ====")

    cv = CountVectorizer(inputCol="cleaned_comment_words", outputCol="cv_words")
    comment_review_format = cv.fit(comment_reviews)
    comment_reviews = comment_review_format.transform(comment_reviews)  # .drop(col("cleaned_comment_words"))

    mh = MinHashLSH(inputCol="cv_words", outputCol="hashes", seed=202, numHashTables=3)
    minhash_fit = mh.fit(comment_reviews)
    comment_reviews = minhash_fit.transform(comment_reviews).cache()

    udf_sa = udf(lambda x: sentiment_analysis(x), FloatType())
    comment_reviews = comment_reviews.filter(col("cleaned_comment_words").isNotNull())
    comment_reviews = comment_reviews.withColumn("sentiment_analysis", udf_sa(col("comments")))

    comment_reviews = comment_reviews.filter(col("sentiment_analysis") != -2)
    comment_reviews = comment_reviews.filter(col("sentiment_analysis").isNotNull())

    print("Transferring hashes...")
    udf_hash = udf(lambda x: "" + str(x[0][0]))
    comment_reviews = comment_reviews.withColumn("hashes_id", udf_hash(col("hashes")))
    comment_reviews = comment_reviews.drop(col("hashes"))
    return comment_reviews


def central(ref_dir, part_size=-1):
    sanity_check = False
    input_ratings_file = ref_dir + os.sep + "input" + os.sep + "airbnb-reviews.csv"
    references_dir = ref_dir + os.sep + "spark-warehouse"
    review_schema = StructType([ \
        StructField("listing_id", IntegerType(), False), \
        StructField("id", IntegerType(), False), \
        StructField("date", DateType(), False), \
        StructField("reviewer_id", IntegerType(), False), \
        StructField("reviewer_name", StringType(), False), \
        StructField("comments", StringType(), False)
    ])
    
    try:
        try:
            spark = SparkSession.builder.master("local[*]") \
                .config("spark.cores.max", "5") \
                .config("spark.driver.memory", "10g") \
                .config("spark.sql.warehouse.dir", references_dir) \
                .enableHiveSupport() \
                .appName("final_joe").getOrCreate()
            hashes_references = spark.read.csv(ref_dir + os.sep + "log_hashes_references", header=True)
            hashes_references.createOrReplaceTempView("log_hashes_references")
            # spark.sql("select * from hash_content limit where hashes_id=154880.0 ").show()
            print("=== WE FOUND THE HASH_REFERENCES! WORKING THROUGH THIS! ===")
            sanity_check = True
        except:
            print("=== WE DID NOT FIND THE HASH_REFERENCES! WILL RUN MinHashLSH AND TERMINATE FOR RE-RUN ===")
            if spark is not None:
                spark.stop()
            spark = SparkSession.builder.master("local[*]") \
                .config("spark.cores.max", "3") \
                .config("spark.driver.memory", "10g") \
                .appName("final_joe").getOrCreate()
        if sanity_check is False:
            print("PARQUET FILES NOT FOUND INITIALLY: WE MUST RUN THE APPLICATION TO PRODUCE THE PARQUET FILES FOR LATER STREAMING...")
            print("-> Restart the program after termination...")
            comment_identify_reviews = spark.read.csv(input_ratings_file, schema=review_schema, sep=";")

            print("Second step is to filter out the languages to only be in English")
            print("  --> This is where the cleaning aspect of our language comes.")
            print("  --> Sentiment Analysis and TFIDFVectorization is performed here.")
            # rev_sample = reviews.withColumnRenamed("listing_id","listing_id_2")
            # comment_identify_reviews = ratings.join(rev_sample, on=ratings.listing_id == rev_sample.listing_id_2, how="inner").drop("listing_id_2")

            # ("2016-01-01","2017-01-01") takes a lot of values, and took over 2 horus and 30 minutes to produce. Reducing to only the summer.
            comment_identify_reviews = comment_identify_reviews.filter(col("comments") != "").filter(col("date").between("2017-01-01", "2022-01-01"))  # .limit(750_000)

            comment_reviews = clean(comment_identify_reviews)

            # comment_reviews_count = comment_reviews.count()
            # print("=============== PRE-Comment Reviews Count: " + str(comment_reviews_count) + "===============")

            hash_write = comment_reviews.select(col("hashes_id")).distinct()

            # comment_reviews_count = comment_reviews.count()
            # print("=============== Comment Reviews Count: "+str(comment_reviews_count) +"===============")
            # return

            # comment_reviews =comment_reviews.withColumn("hashes_id", col("hashes").cast(StringType())).drop(col("hashes"))
            # comment_reviews = comment_reviews.groupBy(col("hashes_id"),col("listing_id"),col("reviewer_id")).agg(collect_list(col("cleaned_comment_words")))

            try:
                hash_write.coalesce(1).write.option("header", True).mode("overwrite").csv(
                    ref_dir + os.sep + "log_hashes_references")

                # comment_reviews = comment_reviews.drop(col("cv_words"))
                comment_reviews = comment_reviews.select(col("listing_id"), col("reviewer_id"), col("comments"),
                                                         col("cleaned_comment_words"), \
                                                         col("hashes_id"), col("cv_words"), col("sentiment_analysis"))

                #print("======= LOG HASH SIZE DF "+ str(comment_reviews.count()) +" =======")
                comment_reviews.write.option("header", True) \
                    .partitionBy("hashes_id") \
                    .mode("overwrite") \
                    .saveAsTable("log_hash_content")

                """
                comment_reviews.write.option("header",True) \
                    .bucketBy(15,"hashes")\
                    .mode("overwrite")\
                    .saveAsTable("hash_content")
                """
            except Exception as e:
                print("Likely already has the spark read directory available for hashes content directory: " + str(e))

            print("Fully computed grouped values...")

            print("==== ENDING STREAMING SAVE ====")
            print("PLEASE RESTART THIS SYSTEM TO ANALYZE THE REST OF THE VALUES LISTED")

            spark.stop()
            return
        if part_size > 0:
            l_col_hash = hashes_references.collect()
            l_col_hash = np.array_split(l_col_hash,part_size)
            resultant = list(map(lambda li : logistic_reg(spark,references_dir+os.sep+"log_hashes_references"+os.sep,li), l_col_hash))
            resultant = np.average(resultant)
        else:
            resultant = logistic_reg(spark,references_dir+os.sep+"log_hashes_references"+os.sep,None)
        print("===== FOUND LOGISTIC REGRESSION IS " + str(resultant) + " =====")

    except Exception as e:
        print("Error: "+str(e))
    finally:
        if spark is not None:
            spark.stop()


if __name__ == '__main__':
    print("This is an add-on file explicitly for logistic regression")
    try:
        if len(sys.argv) < 2:
            print("Requiring at least one arguments, as such: ")
            print("python additional_logistic_reg.py <user directory> <partition size>")
            central("/user/jbrock2")
        elif len(sys.argv) < 3:
            dir_val = str(sys.argv[1])
            central(dir_val)
        elif len(sys.argv) < 4:
            dir_val = str(sys.argv[1])
            part_size = int(sys.argv[2])
            central(dir_val, part_size)
    except Exception as e:
        print("Cannot process; please process with the following arguments")
        print("ERROR: "+str(e))
