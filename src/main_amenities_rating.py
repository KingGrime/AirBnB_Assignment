import os
import re
import sys

import nltk
import numpy as np
from nltk import WordNetLemmatizer
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.clustering import LDA
from pyspark.ml.evaluation import RegressionEvaluator, BinaryClassificationEvaluator
from pyspark.ml.feature import Tokenizer, StopWordsRemover, MinHashLSH, CountVectorizer, IDF, HashingTF
from pyspark.ml.functions import vector_to_array
from pyspark.ml.linalg import VectorUDT
from pyspark.ml.recommendation import ALS
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.sql import SparkSession, Window
from pyspark.sql.functions import col, lower, udf, explode, collect_list, regexp_extract, transform, \
    max as mx, expr, sum as sm, \
    add_months, date_sub, regexp_replace, split, lit, row_number, count, avg, when
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType, DateType, ArrayType

from textblob import TextBlob
from langdetect import detect

from nltk.corpus import words,stopwords
from nltk.sentiment.vader import SentimentIntensityAnalyzer
#nltk.download("words")
nltk.download("stopwords")
nltk.download("wordnet")
nltk.download("vader_lexicon")
wltizer = WordNetLemmatizer()
sid = SentimentIntensityAnalyzer()
wrd = set(words.words())
stopwrd = set(stopwords.words('english'))

# Our hope is to determine the following from the AirBnB dataset:
# -> What are the most useful names, host location, and comments for determining Airbnb ratings data
# -> The best recommendations for places within the United States
# -> OPTIONAL (if there is time): If locality affects the Airbnb Host Response Rate

#There are three stated tools associated with this project
# 1) Utilizing Local Sensitivity Hashing for the purpose of appropriately bucketing AirBNB reviews
# 2) Information from LDA will be processed to determine the most useful topics associated with AirBnB comments
# 3) Determine which USA locations are the best suited for users based on the ratings listed

def lemma(x):
    return wltizer.lemmatize(x).lower() #if x.isalpha() and x in wrd else ''

def print_val(x,data,i):
    retVal = str("==== Topic "+str(i)+" Words ====\n")
    data_i = str(x)
    retVal = retVal+data_i
    return retVal

def central(ref_dir, option = 1, enablePYLDAvis = False):
    spark = None
    try:
        print("Starting data cleaning...")
        references_dir = ref_dir + os.sep + "spark-warehouse"
        spark = SparkSession.builder.master("local[*]")\
                            .config("spark.cores.max", "2")\
                            .config("spark.driver.memory", "5g") \
                            .config("spark.sql.warehouse.dir", references_dir)\
                            .appName("final_joe").getOrCreate()
        input_ratings_file_1 = ref_dir + os.sep + "input" + os.sep + "airbnb_ratings_new.csv"
        output_dir = ref_dir + os.sep + "output"+os.sep
        print("First step is to try to condense this down only into the United States")
        #listing_id;id;date;reviewer_id;reviewer_name;comments
        rating_schema = StructType([ \
            StructField("listing_id", IntegerType(), False), \
            StructField("name", StringType(), False), \
            StructField("host_id", IntegerType(), False), \
            StructField("host_name", StringType(), False), \
            StructField("host_response_rate", StringType(), False), \
            StructField("host_is_superhost", StringType(), False), \
            StructField("host_total_listings_count", StringType(), False), \
            StructField("street", StringType(), False), \
            StructField("city", StringType(), False), \
            StructField("neighborhood_cleansed", StringType(), False), \
            StructField("state", StringType(), False),
            StructField("country", StringType(), False), \
            StructField("latitude", FloatType(), False), \
            StructField("longitude", FloatType(), False), \
            StructField("property_type", StringType(), False), \
            StructField("room_type", StringType(), False), \
            StructField("accommodates", StringType(), False), \
            StructField("bathrooms", IntegerType(), False), \
            StructField("bedrooms", IntegerType(), False), \
            StructField("amenities", StringType(), False), \
            StructField("price", IntegerType(), False), \
            StructField("min_nights", IntegerType(), False), \
            StructField("max_nights", IntegerType(), False), \
            StructField("availability_365", StringType(), False), \
            StructField("calendar_last_scraped", StringType(), False), \
            StructField("no_of_reviews", IntegerType(), False), \
            StructField("last_review_date", StringType(), False), \
            StructField("review_scores_rating", IntegerType(), False), \
            StructField("review_scores_accuracy", FloatType(), False), \
            StructField("review_scores_cleanliness", IntegerType(), False), \
            StructField("review_scores_checkin", IntegerType(), False), \
            StructField("review_scores_com", IntegerType(), False), \
            StructField("review_scores_loc", IntegerType(), False), \
            StructField("reviews_per_month", FloatType(), False)
        ])
        sanity_check = False
        ratings_reviews = None
        try:
            r_schema = StructType([ \
                StructField("listing_id", IntegerType(), False), \
                StructField("reviewer_id", IntegerType(), False), \
                StructField("amenities", StringType(), False), \
                StructField("hashes_id", StringType(), False), \
                StructField('cv_words', VectorUDT(), False) \
                ])
            ratings_reviews = (spark.read.load(ref_dir + os.sep + "spark-warehouse", schema=r_schema))
            sanity_check = True
        except:
            sanity_check = False

        if sanity_check is False:
            ratings = spark\
                    .read\
                    .csv(input_ratings_file_1, header=False, schema=rating_schema)
            ratings = ratings.filter(lower(col("country")) == "united states")

            #FOR TESTING PURPOSES ONLY:
            # -> CSV file will be put forward containing all values listed. (Current process takes 10+ minutes)
            # -> Information will be handled in tested fully
            # -> Any reference to a CSV file will be ignored after completion

            #spark.sparkContext.setCheckpointDir(ref_dir+os.sep+"checkpoints")

            print("Second step is to filter out the languages to only be in English")
            print("  --> This is where the cleaning aspect of our language comes.")
            print("  --> Sentiment Analysis and TFIDFVectorization is performed here.")
            #rev_sample = reviews.withColumnRenamed("listing_id","listing_id_2")
            #comment_identify_reviews = ratings.join(rev_sample, on=ratings.listing_id == rev_sample.listing_id_2, how="inner").drop("listing_id_2")
            udf_lemma = udf(lambda x: "" + lemma(x), StringType())
            ratings = ratings.filter(col("amenities") != "").filter(col("amenities").isNotNull()) #Might need to include higher rating accuracy
            ratings = ratings.select(col("listing_id"),col("host_id"),explode(split(col("amenities"),pattern=";")).alias("amenities"),col("review_scores_rating").alias("rating"))
            ratings = ratings.withColumn("c_amenities", udf_lemma(lower(col("amenities"))))
            ratings = ratings.filter(col("c_amenities") != "")
            ratings = ratings.groupBy(col("listing_id"),col("host_id"),col("rating")).agg(collect_list("c_amenities").alias("amenities")).dropna()

            print("First, it's a good idea to partition based on the comments. This'll be useful for our LDA, but also our minhash and recommender later")

            print("=== MIN HASH APPLICATION ===")
            print("Fourth step is to use MinHash for LSH to determine how different the past Dataframe was.")
            print("  --> This helps us identify how different both Dataframes truly are.")
            print("  --> It also helps identify how much has changed")
            #This is based on what's found under ML features for pyspark: https://spark.apache.org/docs/2.2.3/ml-features.html#locality-sensitive-hashing

            cv = CountVectorizer(inputCol="amenities", outputCol="cv_words")
            ratings_reviews_format = cv.fit(ratings)
            ratings_reviews = ratings_reviews_format.transform(ratings) #.drop(col("cleaned_comment_words"))

            mh = MinHashLSH(inputCol="cv_words",outputCol="hashes",seed=202,numHashTables=3)
            minhash_fit = mh.fit(ratings_reviews)
            ratings_reviews = minhash_fit.transform(ratings_reviews).cache()

            udf_hash = udf(lambda x : ""+str(x[0][0])) #+"_"+str(x[0][1])+"_"+str(x[0][2])
            ratings_reviews = ratings_reviews.withColumn("hashes_id", udf_hash(col("hashes"))).drop(col("hashes"))

            #hash_write = ratings_reviews.select(col("hashes_id")).distinct()

            #There will be two written files: One regarding hashing, and one regarding the actual based on those hashes
            try:
                #hash_write.coalesce(1).write.option("header", True).mode("overwrite").csv(ref_dir + os.sep + "hashes_references")

                ratings_reviews.write.option("header",True)\
                    .partitionBy("hashes_id")\
                    .mode("overwrite")\
                    .saveAsTable("hash_content")
                """
                ratings_reviews.write.option("header",True) \
                    .bucketBy(35,"hashes")\
                    .mode("overwrite")\
                    .saveAsTable("hash_content")
                """
            except Exception as e:
                print("ERROR: Likely already has the spark read directory available for hashes content directory: "+str(e))

            print("=== COMPLETED MIN HASH ===")
            #Here, we'll group all of the values to be used later for streaming

            return
        """
        if option == 0:
            print("=== LDA RETRIEVAL ===")
            print("Third step is to grab the most useful topics from LDA, and then filter the Dataframe based on the topics found")
            ret_comments = ratings_reviews.select(col("amenities").alias("words")).collect()
            ret_comments = list(map(lambda x : x["words"],ret_comments))
            lda_model = LDA(k=15, learningDecay=0.1,seed=202, featuresCol="cv_words")
            lda_fit = lda_model.fit(ratings_reviews)
            lda_output = lda_fit.transform(ratings_reviews)
            crf_vocab = ratings_reviews_format.vocabulary

            print("Finished LDA... ")

            lda_desc_topics = lda_fit.describeTopics().select("topic","termIndices").collect()
            lda_desc_topics = list(map(lambda x: (x["topic"], list(map(lambda y: str(crf_vocab[y]), x["termIndices"]))), lda_desc_topics))
            #print value is taking up a lot of time. May require threading
            lda_tpcs = list(map(lambda x : print_val(x[1],ret_comments,x[0]),lda_desc_topics))
            print("\n".join(lda_tpcs))

            print("Finished printing LDA values... ")


            if enablePYLDAvis == True:
                print("Providing pyLDAvis data...")
                topics = np.asarray(lda_fit.topicsMatrix().toArray()).T

                lda_output = lda_output.withColumn("iterate",lit(1)).drop(col("cv_words"))
                win_rn = Window.partitionBy("iterate").orderBy("iterate")
                lda_output = lda_output.withColumn("row_num", row_number().over(win_rn)).drop("iterate")
                lda_total = lda_output.select(col("row_num"),explode(col("amenities")).alias("words"))

                lda_total_words = lda_total.groupBy(col("row_num")).agg(count(col("words")).alias("total_words_count"))

                lda_obj_tf_frequency = lda_total.select(col("words")).groupBy(col("words")).agg(count("words").alias("freq"))
                lda_obj_tf_frequency = lda_obj_tf_frequency.collect()
                lda_obj_tf_frequency = dict(map(lambda x: (x["words"], x["freq"]), lda_obj_tf_frequency))

                allowed_row_nums = lda_output.select(col("row_num"),
                                                     explode(vector_to_array(col("topicDistribution"))).alias("topic_dist"))
                allowed_row_nums = allowed_row_nums.groupBy(col("row_num")).agg(sm(col("topic_dist")).alias("topic_dist_sum"))
                allowed_row_nums = allowed_row_nums.filter(col("topic_dist_sum") == 1).collect()
                allowed_row_nums = list(map(lambda x: x["row_num"], allowed_row_nums))

                docTerm = lda_output.filter(col("row_num").isin(allowed_row_nums)).select(
                    col("topicDistribution")).collect()
                lda_total_words = lda_total_words.filter(col("row_num").isin(allowed_row_nums)).select(col("row_num"),
                                                                                                       col("total_words_count"))\
                    .orderBy(col("row_num")).collect()
                docTerm = np.array(list(map(lambda x: x["topicDistribution"].toArray(), docTerm)))
                docLen = np.array(list(map(lambda x: x["total_words_count"], lda_total_words)))
                vocab_freq = np.array(list(map(lambda x: lda_obj_tf_frequency[x], crf_vocab)))

                print("Finished pylDAvis Prep... ")
                try:
                    import pyLDAvis
                    from pyLDAvis import sklearn
                    prepped = pyLDAvis.prepare(topics, docTerm, docLen, crf_vocab, vocab_freq)
                    pyLDAvis.save_html(prepped, output_dir+"LDA_Results.html")
                    print("Finished pyLDAvis... ")
                except Exception as e:
                    print("Could not execute pyLDAvis: "+str(e))
            
            lda_output = lda_output.withColumn("throwaway", lit(1))
            win_rn = Window.partitionBy(col("throwaway")).orderBy(col("host_id"))
            lda_output = lda_output.withColumn("row_no", row_number().over(win_rn))
            lda_output = lda_output.drop(col("throwaway"))
            lda_output = lda_output.withColumn("label",when(col("rating") >= 6,1).otherwise(0))
    
            train_job = lda_output.sample(fraction=0.7,seed=202)
            test_job = lda_output.drop(train_job.row_no)
            train_job_pre = train_job.select(col("cv_words").alias("features"), col("label"))
            test_job_pre = test_job.select(col("cv_words").alias("features"), col("label"))
    
            lr = LogisticRegression()
            
            paramGrid = ParamGridBuilder() \
                .addGrid(lr.regParam, [0.1, 0.01]).build()  # Remove numFeats if needed
            # .addGrid(rdd_description_hash_sp.numFeatures, [10,100,1000])\
    
            print("Working through Logistic Regression...")
    
            cv = CrossValidator(estimator=lr,
                                estimatorParamMaps=paramGrid,
                                evaluator=BinaryClassificationEvaluator(),
                                numFolds=3, parallelism=5)
            
            lrModelTrain = cv.fit(train_job_pre)
            lrModelTest = lrModelTrain.transform(test_job_pre)
    
            print("Finished Log Regression without topicDistribution... ")
    
            lrROCClass = BinaryClassificationEvaluator(rawPredictionCol="rawPrediction",metricName="areaUnderROC")
            resultant_ROC = lrROCClass.evaluate(lrModelTest)
            print("==== LR Model ROC Evaluation ====")
            print("LR RESULTS: "+str(resultant_ROC))
            """
        # #This is where we have to start our recommender system journey
        # als_format = ALS(seed=202, coldStartStrategy="drop", nonnegative=True, rank=10, regParam=0.5,userCol="host_id",itemCol="listing_id",ratingCol="rating")
        # als_format = als_format.setImplicitPrefs(False)
        # #print("ATTEMPTING TO FIT ALS")
        # als_fit = als_format.fit(ratings_reviews)
        # #print("ATTEMPTING TO TRANSFORM ALS")
        # ratings_reviews = als_fit.transform(ratings_reviews)

        """
        ratings_reviews_only = ratings_reviews.select(col("listing_id"),col("host_id"), col("cv_words"), col("amenities"), col("prediction").alias("ALS_prediction"), col("rating")) #,col("prediction").alias("ALS_prediction")
        ratings_reviews_dup = ratings_reviews.select(col("listing_id").alias("l2"), col("cv_words"), col("host_id").alias("h2"), col("amenities").alias("a2"), col("rating"))

        ratings_hash_reviews = minhash_fit.approxSimilarityJoin(ratings_reviews_only,\
                                                                ratings_reviews_dup,\
                                                                0.1,distCol="CosineDistance")
        ratings_hash_reviews = ratings_hash_reviews.select(col("datasetA.listing_id").alias("const_listing"),col("datasetB.l2").alias("sim_listing"),\
                                                           col("datasetA.host_id").alias("host_id"),col("datasetB.h2").alias("h2"),
                                                           col("datasetA.amenities").alias("amenities"),col("datasetB.a2").alias("amenities_2"),
                                                           col("datasetA.rating").alias("rating"),col("datasetB.rating").alias("rating_2"), col("CosineDistance").alias("similarity_score"),
                                                           col("datasetA.ALS_prediction").alias("ALS_prediction"))

        ratings_hash_reviews = ratings_hash_reviews.filter(ratings_hash_reviews.host_id == ratings_hash_reviews.h2)\
                                                   .filter(ratings_hash_reviews.const_listing < ratings_hash_reviews.sim_listing)\
                                                   .filter(ratings_hash_reviews.rating_2.isNotNull())\
                                                   .filter(ratings_hash_reviews.rating.isNotNull()).drop(col("h2")).dropna()

        #We must provide a recommendation system based on rating and locality.
        print("=== RECOMMENDER SYSTEM ===")
        print("Fifth step is to identify a recommender system using the listing id, reviewer id, positive/negative review, and distance")
        print("  --> Sentiment Analysis results is utilized here.")

        item_id_spec = Window.partitionBy(col("const_listing"))
        item_id2_spec = Window.partitionBy(col("sim_listing"))
        item_spec = Window.partitionBy(col("const_listing"), col("sim_listing"))
        usr_rat_win = Window.partitionBy(col("host_id"))

        ratings_hash_reviews = ratings_hash_reviews.withColumn("sum_sim_ij", sm(col("similarity_score")).over(item_spec))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("user_reviews", count(col("host_id")).over(usr_rat_win))

        coOccurenceThreshold = 40
        ratings_hash_reviews = ratings_hash_reviews.filter(col("user_reviews") >= coOccurenceThreshold)
        avg_rating = ratings_hash_reviews.groupBy(col("rating")).agg(avg("rating").alias("avg_rating")).first()["avg_rating"]
        ratings_hash_reviews = ratings_hash_reviews.withColumn("usr_avg_ratings", avg(col("rating")).over(usr_rat_win))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("avg_rating", lit(avg_rating))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("usr_avg_ratings", col("usr_avg_ratings") - col("avg_rating"))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("item_j_avg_ratings",avg(col("rating")).over(item_id_spec))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("item_i_avg_ratings", avg(col("rating_2")).over(item_id2_spec))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("bxi", col("avg_rating") + (col("usr_avg_ratings") - col("avg_rating")) + (
                col("item_i_avg_ratings") - col("avg_rating")))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("bxj", col("avg_rating") + (col("usr_avg_ratings") - col("avg_rating")) + (
                col("item_j_avg_ratings") - col("avg_rating")))

        ratings_hash_reviews = ratings_hash_reviews.withColumn("numerator",
                                         sm(col("similarity_score") * (col("rating") - col("bxj"))).over(item_id_spec))
        ratings_hash_reviews = ratings_hash_reviews.withColumn("r_pred", (col("bxi") + (col("numerator") / col("sum_sim_ij"))))
        r_final = ratings_hash_reviews.select(col("host_id"), col("sim_listing").alias("listing"), col("rating_2").alias("rating"), col("r_pred").alias("itemCF_prediction"), col("ALS_prediction"))
        r_final = r_final.withColumn("hybrid_pred", (col("ALS_prediction") * 0.7) + (col("itemCF_prediction") * 0.3)).drop(*("ALS_prediction","itemCF_prediction")).dropna()
        """

        # reg_eval = RegressionEvaluator(metricName="rmse", labelCol="rating", predictionCol="prediction") #hybrid_pred
        # ratings_rmse = reg_eval.evaluate(ratings_reviews) #r_final

        # print("======Recommender Results======")
        # print("Hybrid RMSE results: " + str(ratings_rmse))

        # reg_eval = RegressionEvaluator(metricName="mse", labelCol="rating", predictionCol="prediction") #hybrid_pred
        # ratings_mse = reg_eval.evaluate(ratings_reviews) #r_final
        # print("Hybrid MSE results: " + str(ratings_mse))

        # reg_eval = RegressionEvaluator(metricName="r2", labelCol="rating", predictionCol="prediction") #hybrid_pred
        # ratings_mse = reg_eval.evaluate(ratings_reviews)
        # print("Hybrid R2 results: " + str(ratings_mse))


    except Exception as e:
        print("An exception occurred: "+str(e))
    finally:
        print("Terminated Spark Instance")
        if spark is not None:
            spark.stop()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Requiring at least two arguments, as such: ")
        print("python main.py <user directory> <enable pyLDAvis>")
        central("/user/jbrock2")
    elif len(sys.argv) < 3:
        dir_val = str(sys.argv[1])
        central(dir_val)
    elif len(sys.argv) < 4:
        dir_val = str(sys.argv[1])
        en_pyl = str(sys.argv[2]).lower() == 'true'
        central(dir_val,en_pyl)
    else:
        a_dir = str(sys.argv[1])
        central(a_dir)
