Assignment 4
By: Joe Brock

Prior to running this program, please perform the following steps...
1) First, we must do a sanity check on what our application setup is working as. You should have the
following before starting.
    - The /src directory, containing a4.py
        + DO NOT REMOVE THESE FROM THEIR DIRECTORY
        + PLEASE run these files out of the source directory.
    - The /output directory
        + These files can be removed; they simply provide insight into what the data should look like.
    - The /input directory
        + The input directory does NOT contain the files being used.
        + HOWEVER, you MUST download them from the following site: https://www.kaggle.com/datasets/samyukthamurali/airbnb-ratings-dataset?select=airbnb-reviews.csv
        + Download all files, but the one that gain priority is airbnb-ratings.csv
        + Place them into your directory

2) Make sure that the following packages are installed via pip for your python environment:
    - re
    - pyspark
    - pyspark.sql
    - nltk
    - TextBlob

3) For the next steps, it is important that you understand which environment you're working with:
    - If working with the Perseus Environment, follow the steps listed in 3A
    - If working with the localhost environment, ensure that when running main_reviews.py that
    you are referencing the files from YOUR own local directory
        + For example, my local directory is /home//PycharmProjects/CS657,
        so I would enter that when prompted under main_reviews.py

    If using the Perseus environment, pass over the zipped file to the scp environment using the following commands:
    - Here are those commands:
        scp -r BrockJOB_BlasJoelle_FinalProject.zip jbrock2@perseus.vsnet.gmu.edu:
        unzip BrockJOB_BlasJoelle_FinalProject.zip
    - For localhost environment, "unzip BrockJOB_BlasJoelle_Assignment4.zip" is fine.
    - IMPORTANT: The input file will take a long time, so it is recommended to scp the files individually into your
    perseus environment

3A) If you are using perseus, it is necessary to ensure that an input and output folder have been created.
Login first through ssh and your password.
Then, check your status with the following commands:
    hdfs dfs -ls -r /user/
Any input and output directories should be deleted prior to use. If there are any folders, perform the following actions:
    hdfs dfs -rm -r /user/<include username>/input
    hdfs dfs -rm -r /user/<include username>/output
To remake these directories, do this:
    hdfs dfs -mkdir /user//input
    hdfs dfs -mkdir /user//output
...and then on another tab, move information from scp to hdfs using the following command (on another tab)

    hdfs dfs -copyFromLocal ./BrockJOB_BlasJoelle_FinalProject/input/* /user/<username>/input

    (This will take approximately 20 minutes)

4) a4.py can then be run out of the src folder (IMPORTANT: DO NOT INCLUDE A "/" AT THE END OF THE FILE PATH):
    For spark-submit under Perseus:

    spark-submit --driver-memory 10G --executor-memory 1G --num-executors 1 main_reviews.py /user/<include username>  > results.txt
    spark-submit --driver-memory 10G --executor-memory 1G --num-executors 1 additional_logistic_reg.py /user/<include username> <Number of Partitions> > results.txt

    Do NOT use your local environment


The information listed will be under your console output, hence why spark submit has the output ">" present

5) Afterwards, results will typically be towards the end of the generated text file, but here are searchable phrases for your values:

For Logistic Regression Model, search by this phrase...
=== LR Model Evaluation Result:

For LDA, search using this phrase...
=== LDA PROCESS IS BEING RUN ===

For CF, searching using this phrase...
=== CF PROCESS IS COMPLETED; NOW SHOWING ==